package com.example.gpslogger;

import java.util.Collection;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException.ConnectionException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.sasl.SASLErrorException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.util.XmlStringBuilder;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.os.Parcelable;

import com.example.gpslogger.MainActivity.ConnectionStatusValues;

public class XMPPService extends Service {

	private XMPPTCPConnection connection;
	private Thread th = null;
	private Integer sleepInterval = 5000;
	private GPSTracker gps;
	private String serverAddress;
	private String userLogin;
	private String userPassword;
	private PacketListener myListener;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		serverAddress = intent.getExtras().getString(MainActivity.SERVER_ADDRESS);
		userLogin = intent.getExtras().getString(MainActivity.USER_LOGIN);
		userPassword = intent.getExtras().getString(MainActivity.USER_PASSWORD);
		gps = new GPSTracker(this);
		return Service.START_NOT_STICKY;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		th = new Thread() {
			Intent intent = new Intent(MainActivity.DATA_ARRIVED_INTENT);

			public void run() {
				try {
					SASLAuthentication.supportSASLMechanism("PLAIN", 0);
					ConnectionConfiguration connConfig = new ConnectionConfiguration(serverAddress);
					connConfig.setSecurityMode(SecurityMode.disabled);
					connConfig.setCompressionEnabled(false);
					connConfig.setDebuggerEnabled(true);
					connConfig.setReconnectionAllowed(false);
					connConfig.setSendPresence(false);
					connection = new XMPPTCPConnection(connConfig);
					connection.connect();
					connection.login(userLogin, userPassword);
					PacketFilter filter = new org.jivesoftware.smack.filter.AndFilter(new PacketTypeFilter(
							Message.class));
					myListener = new PacketListener() {
						public void processPacket(Packet packet) {
							Message message = (Message) packet;
							String messageBody = message.getBody();
							if (messageBody == null) {
								messageBody = "";
								Collection<Message.Body> bodies = message.getBodies();
								for (Message.Body r : bodies) {
									messageBody += r.getMessage();
								}
							}
							if ("interval".equals(message.getSubject())) {
								intent.putExtra(MainActivity.REPEATE_INTERVAL, messageBody);
								sendStickyBroadcast(intent);
								sleepInterval = 1000 * Integer.parseInt(messageBody);
							}
						}
					};
					Presence presenceStart = new Presence(Presence.Type.available);
					presenceStart.setStatus("start");
					connection.sendPacket(presenceStart);
					connection.addPacketListener(myListener, filter);
					while (connection.isConnected()) {
						intent.putExtra(MainActivity.CONNECTION_STATUS, (Parcelable)ConnectionStatusValues.CONNECTED);
						GPSTracker.setMIN_TIME_BW_UPDATES(sleepInterval);
						if (gps.canGetLocation()) {
							Location location = gps.getLocation();
							if (location == null) {
								intent.putExtra(MainActivity.DEBUG_INFO, "waiting for gps");
								Presence presence = new Presence(Presence.Type.available);
								presence.setStatus("waiting for gps");
								connection.sendPacket(presence);
							} else {
								String latitude = Double.toString(location.getLatitude());
								String longitude = Double.toString(location.getLongitude());
								CustomPresence presence = new CustomPresence(Presence.Type.available) {
								};
								String locMessage = "<status>coordinates</status><coordinates><lat>" + latitude
										+ "</lat><lon>" + longitude + "</lon></coordinates>";
								presence.setCustomStanza(locMessage);
								connection.sendPacket(presence);
								intent.putExtra(MainActivity.DEBUG_INFO, "ok");
								intent.putExtra(MainActivity.COORDINATES_LAT, latitude);
								intent.putExtra(MainActivity.COORDINATES_LONG, longitude);
							}
						} else {
							intent.putExtra(MainActivity.DEBUG_INFO, "gps error");
							Presence presence = new Presence(Presence.Type.available);
							presence.setStatus("gps error");
							connection.sendPacket(presence);
						}
						sendStickyBroadcast(intent);
						Thread.sleep(sleepInterval);
					}
				} catch (Exception e) {
					if (e instanceof NotConnectedException) {
						intent.putExtra(MainActivity.CONNECTION_STATUS, (Parcelable)ConnectionStatusValues.NOTCONNECTEDERROR);
					} else if (e instanceof SASLErrorException) {
						intent.putExtra(MainActivity.CONNECTION_STATUS, (Parcelable)ConnectionStatusValues.AUHORIZATIONERROR);
					} else if (e instanceof ConnectionException) {
						intent.putExtra(MainActivity.CONNECTION_STATUS, (Parcelable)ConnectionStatusValues.CONNECTIONERROR);
					} else {
						intent.putExtra(MainActivity.CONNECTION_STATUS, (Parcelable)ConnectionStatusValues.ERROR);
					}
					intent.putExtra(MainActivity.DEBUG_INFO, e.getMessage());
					sendBroadcast(intent);
				}
			}
		};
		th.start();
	}

	@Override
	public void onDestroy() {
		new Thread() {
			public void run() {
				forceDisconnection();
			}
		}.start();
		th = null;
	}
	
	private void forceDisconnection() {
		Intent intent = new Intent(MainActivity.DATA_ARRIVED_INTENT);
		Presence presenceStop = new Presence(Presence.Type.available);
		presenceStop.setStatus("stopped");
		if (connection != null && connection.isConnected()) {
			try {
				if (myListener != null) {
					connection.removePacketListener(myListener);
				}
				connection.disconnect(presenceStop);
				intent.putExtra(MainActivity.CONNECTION_STATUS, (Parcelable)ConnectionStatusValues.DISCONNECTED);
				intent.putExtra(MainActivity.DEBUG_INFO, "stopped");
			} catch (NotConnectedException e) {
				intent.putExtra(MainActivity.CONNECTION_STATUS, (Parcelable)ConnectionStatusValues.NOTCONNECTEDERROR);
				intent.putExtra(MainActivity.DEBUG_INFO, e);
				e.printStackTrace();
			}
			sendBroadcast(intent);
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private class CustomPresence extends org.jivesoftware.smack.packet.Presence {

		public CustomPresence(Type type) {
			super(type);
		}

		private String customStanza;

		public void setCustomStanza(String customStanza) {
			this.customStanza = customStanza;
		}

		@Override
		public XmlStringBuilder toXML() {
			XmlStringBuilder XMLMessage = super.toXML();
			XmlStringBuilder XMLresult = new XmlStringBuilder();
			XMLresult.append(XMLMessage.subSequence(0, XMLMessage.length() - 11));
			XMLresult.append(customStanza);
			XMLresult.append("</presence>");
			return XMLresult;
		}
	}

}
