package com.example.gpslogger;

import org.jivesoftware.smack.SmackAndroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	final static String SERVER_ADDRESS = "com.example.gpslogger.server_address";
	final static String USER_LOGIN = "com.example.gpslogger.user_login";
	final static String USER_PASSWORD = "com.example.gpslogger.user_password";
	final static String REPEATE_INTERVAL = "com.example.gpslogger.interval";
	final static String DEBUG_INFO = "com.example.gpslogger.debug_info";
	final static String CONNECTION_STATUS = "com.example.gpslogger.connection_status";
	final static String COORDINATES_LAT = "com.example.gpslogger.coordinates_lat";
	final static String COORDINATES_LONG = "com.example.gpslogger.coordinates_long";
	final static String DATA_ARRIVED_INTENT = "com.example.gpslogger.DATA_ARRIVED";
	
	public enum ConnectionStatusValues implements Parcelable {
		CONNECTED, DISCONNECTED, CONNECTIONERROR, NOTCONNECTEDERROR, ERROR, AUHORIZATIONERROR;
		@Override
		public String toString() {
			switch (this) {
			case CONNECTED:
				return "connected";
			case DISCONNECTED:
				return "disconnected";
			case CONNECTIONERROR:
				return "connection error";
			case NOTCONNECTEDERROR:
				return "not connected";
			case ERROR:
				return "error";
			case AUHORIZATIONERROR:
				return "authorization error";
			}
			return null;
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeInt(ordinal());
		}

		public static final Creator<ConnectionStatusValues> CREATOR = new Creator<ConnectionStatusValues>() {
			@Override
			public ConnectionStatusValues createFromParcel(final Parcel source) {
				return ConnectionStatusValues.values()[source.readInt()];
			}

			@Override
			public ConnectionStatusValues[] newArray(final int size) {
				return new ConnectionStatusValues[size];
			}
		};

	};

	private boolean buttonStartState;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		SmackAndroid.init(this);
		registerReceiver(uiUpdated, new IntentFilter(DATA_ARRIVED_INTENT));
	    SharedPreferences settings = getPreferences(0);
		if (settings != null) {
			((EditText) findViewById(R.id.server_address)).setText(settings.getString("saved_server_address", ""));
			((EditText) findViewById(R.id.login)).setText(settings.getString("saved_login", ""));
			((EditText) findViewById(R.id.password)).setText(settings.getString("saved_password", ""));
		}
		Button startButton = (Button) findViewById(R.id.button1);
		startButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				if (!buttonStartState) {
					((TextView) findViewById(R.id.debug_info)).setText("Started");
					EditText serverAddress = (EditText) findViewById(R.id.server_address);
					EditText userLogin = (EditText) findViewById(R.id.login);
					EditText userPassword = (EditText) findViewById(R.id.password);
					Intent intent = new Intent(MainActivity.this, XMPPService.class);
					intent.putExtra(SERVER_ADDRESS, serverAddress.getText().toString());
					intent.putExtra(USER_LOGIN, userLogin.getText().toString());
					intent.putExtra(USER_PASSWORD, userPassword.getText().toString());
					startService(intent);
				} else {
					stopXMPPService();
				}
				buttonStartState = !buttonStartState;
				setStartButtonValue();
			}
		});
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean("buttonStartState", buttonStartState);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		buttonStartState = savedInstanceState.getBoolean("buttonStartState");
		setStartButtonValue();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		SharedPreferences settings = getPreferences(0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("saved_server_address", ((EditText) findViewById(R.id.server_address)).getText().toString());
		editor.putString("saved_login", ((EditText) findViewById(R.id.login)).getText().toString());
		editor.putString("saved_password", ((EditText) findViewById(R.id.password)).getText().toString());
		editor.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		stopXMPPService();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setStartButtonValue() {
		((Button) findViewById(R.id.button1)).setText(buttonStartState ? "Stop" : "Start");
	}
	
	private void stopXMPPService() {
		Intent intent = new Intent(MainActivity.this, XMPPService.class);
		stopService(intent);
	}

	private BroadcastReceiver uiUpdated = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(isInitialStickyBroadcast())
				return;
			String debugInfo = intent.getExtras().getString(DEBUG_INFO);
			String coordinatesLat = intent.getExtras().getString(COORDINATES_LAT);
			String coordinatesLong = intent.getExtras().getString(COORDINATES_LONG);
			String interval = intent.getExtras().getString(REPEATE_INTERVAL);
			ConnectionStatusValues connectionStatus = intent.getParcelableExtra(CONNECTION_STATUS);
			if (connectionStatus != null) {
				((TextView) findViewById(R.id.connection_status)).setText(connectionStatus.toString());
				if (connectionStatus != ConnectionStatusValues.CONNECTED) {
					buttonStartState = false;
					setStartButtonValue();
					stopXMPPService();
				}
			}
			if (debugInfo != null)
				((TextView) findViewById(R.id.debug_info)).setText(debugInfo);
			if (coordinatesLat != null)
				((TextView) findViewById(R.id.coordinates_lat)).setText(coordinatesLat);
			if (coordinatesLong != null)
				((TextView) findViewById(R.id.coordinates_long)).setText(coordinatesLong);
			if (interval != null)
				((TextView) findViewById(R.id.repeat_interval)).setText(interval);
		}
	};

}
